FROM sfepy/sfepy-notebook

WORKDIR /home/sfepy

COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
