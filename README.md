# SfePy Docker compose

A Docker-compose file to run [SfePy](https://sfepy.org/doc-devel/index.html), start [Jupyter Lab](https://jupyter.org/), and map the local directory to the SfePy container.

## Getting started

The following sections describe how to prepare and run this project.

###  Install dependencies
In order to run this project, you will need to install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/). Optionally, install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) so that you can clone this repository.

### Get the code

You can get the code from this repository either by

- downloading and extracting to a local folder
- using Git to clone to a local folder

For brevity, the details of the above options will be left to the reader.

### Run the project

Once you have obtained the code, you can run the project by the following steps.

1. open a terminal (command line)
2. change into the project directory (e.g. with the `cd` command)
3. run the command `docker-compose up`

The Docker Composee file will start the `sfepy/sfepy-notebook` container and run Jupyter Lab.

### Access Jupyter Lab

After the project is running, you will see some instructions in your terminal describing how to access the server. Copy and paste the appropriate URL into your web browser (e.g. `http://127.0.0.1:8888/lab?token=...`). You should see the Jupyter Lab loading icon and then an interactive code environment.

### Start editing code

Once you have accessed Jupyter Lab, you can create and edit notebooks directly in the browser. All of your notebooks will be saved in your local filesystem in the project directory.

### Add custom Python requirements

At any time, you can add custom Python requirements by editing `requirements.txt`.

### Rebuilding the Docker images

When changing the contents of the `requirements.txt`, you will need to rebuild the Docker image using the command `docker-compose build`.
